import fs from 'fs-extra'; 
import url from 'url';
import express from 'express';
import hbs from 'hbs';
import path from 'path';
import util from 'util';
import favicon from 'serve-favicon';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import DBG from 'debug';
const debug = DBG('notes:notes-level'); 
const error = DBG('notes:error-level'); 
import rfs from 'rotating-file-stream';
import morgan from 'morgan'

//--------------------SESSION-----------------------------

import session from 'express-session';
import sessionFileStore from 'session-file-store';
const FileStore = sessionFileStore(session); 
export const sessionCookieName = 'notescookie.sid';

//--------------------ROUTES-----------------------------

import { router as index } from './routes/index';
import { router as users, initPassport } from './routes/users';
import { router as notes } from './routes/notes';  

// Workaround for lack of __dirname in ES6 modules
const __dirname = path.dirname(new URL(import.meta.url).pathname);


// ---------------------------LOGGIN--------------------------------------

var logStream;
// registro del log en un archivo
if (process.env.REQUEST_LOG_FILE) {
  (async () => {
    let logDirectory = path.dirname(process.env.REQUEST_LOG_FILE); 
    await fs.ensureDir(logDirectory); // Asegura que el directorio exista. Si la estructura del directorio no existe, se crea. Al igual mkdir -p.
    logStream = rfs(process.env.REQUEST_LOG_FILE, {
        size: '10M',     // rotate every 10 MegaBytes written
        interval: '1d',  // rotate daily
        compress: 'gzip' // compress rotated files
    });
  })().catch(err => { console.error(err); });
}

// ---------------------------Initial App--------------------------------------

const app = express();

// ---------------------------view engine setup-------------------------------------- 

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
hbs.registerPartials(path.join(__dirname, 'partials'));
app.use(logger(process.env.REQUEST_LOG_FORMAT || 'dev', { //LOGGIN
  stream: logStream ? logStream : process.stdout 
})); 
app.use(logger(process.env.REQUEST_LOG_FORMAT || 'dev')); 
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(morgan('dev'))
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
//app.use('/assets/vendor/bootstrap', express.static(path.join(__dirname, 'node_modules', 'bootstrap', 'dist')));
//app.use('/assets/vendor/bootstrap', express.static(path.join(__dirname, 'themes', 'bootstrap-4-dev', 'dist')));
app.use('/assets/vendor/jquery', express.static(path.join(__dirname, 'node_modules', 'jquery')));
app.use('/assets/vendor/popper.js', express.static(path.join(__dirname, 'node_modules', 'popper.js', 'dist')));
app.use('/assets/vendor/popper.js', express.static(path.join(__dirname, 'node_modules', 'papercssp', 'dist')));
app.use('/assets/vendor/feather-icons', express.static(path.join(__dirname, 'node_modules', 'feather-icons', 'dist')));
app.use('/assets/vendor/bootstrap/js', express.static(path.join(__dirname, 'node_modules', 'bootstrap', 'dist', 'js')));
app.use('/assets/vendor/bootstrap/css', express.static(path.join(__dirname, 'minty')));

//---------------SESSION-----------------------------------------------

app.use(session({ 
  store: new FileStore({ path: "sessions" }), 
  secret: 'keyboard mouse',
  resave: true,
  saveUninitialized: true,
  name: sessionCookieName
})); 
initPassport(app);

// ---------------------------Routes--------------------------------------
app.use('/', index);
app.use('/notes', notes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// ---------------------------Uncaught exceptions--------------------------------------


process.on('uncaughtException', function(err) { 
  error("I've crashed!!! - "+ (err.stack || err)); 
}); 

process.on('unhandledRejection', (reason, p) => {
  error(`Unhandled Rejection at: ${util.inspect(p)} reason: ${reason}`);
});

if (app.get('env') === 'development') { 
  app.use(function(err, req, res, next) { 
    // util.log(err.message); 
    res.status(err.status || 500); 
    error((err.status || 500) +' '+ error.message); 
    res.render('error', { 
      message: err.message, 
      error: err 
    }); 
  }); 
} 


app.use(function(err, req, res, next) { 
  // util.log(err.message); 
  res.status(err.status || 500); 
  error((err.status || 500) +' '+ error.message); 
  res.render('error', { 
    message: err.message, 
    error: {} 
  }); 
});

export default app;
