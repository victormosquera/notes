import fs from 'fs-extra';
import path from 'path';
import util from 'util';
import Note from './Note';
import DBG from 'debug';
const debug = DBG('notes:notes-fs');
const error = DBG('notes:error-fs');

async function notesDir() { 
    const dir = process.env.NOTES_FS_DIR || "notes-fs-data"; 
    await fs.ensureDir(dir);
    return dir;
} 

function filePath(notesdir, key) { return path.join(notesdir, `${key}.json`); } 

async function readJSON(notesdir, key) { 
    const readFrom = filePath(notesdir, key); 
    var data = await fs.readFile(readFrom, 'utf8');
    return Note.fromJSON(data);
}

/**
 La notesDir función se utilizará en todo momento notes-fs para garantizar que el directorio exista. 
 Para simplificar esto, estamos usando el fs-extra módulo porque agrega funciones basadas en la promesa 
 al  fs módulo ( https://www.npmjs.com/package/fs-extra ). En este caso, fs.ensureDir verifica si existe
  la estructura de directorios nombrada y, de no ser así, se crea la ruta del directorio. 

La variable de entorno,, NOTES_FS_DIRconfigura un directorio dentro del cual almacenar notas. Tendremos un
 archivo por nota y almacenaremos la nota como JSON. Si no se especifica una variable de entorno, volveremos
  a utilizar notes-fs-datacomo el nombre del directorio.
 */

async function crupdate(key, title, body) { 
    var notesdir = await notesDir(); //verifica que el directorio exista
    if (key.indexOf('/') >= 0) 
        throw new Error(`key ${key} cannot contain '/'`); 
    var note = new Note(key, title, body); 
    const writeTo = filePath(notesdir, key); 
    const writeJSON = note.JSON; //comvierte a  JSON la nota
    await fs.writeFile(writeTo, writeJSON, 'utf8'); //guarda el archivo el la ruta (writeTo) y el archivo (writeJSON)
    return note;
}

export function create(key, title, body) { return crupdate(key, title, body); }
export function update(key, title, body) { return crupdate(key, title, body); }

export async function read(key) { 
    var notesdir = await notesDir(); //verifica que el directorio exista
    var thenote = await readJSON(notesdir, key); //lee el archivo y retorna el archivo convertido En JSON
    return thenote; 
}

export async function destroy(key) { 
    var notesdir = await notesDir();  //verifica que el directorio exista
    await fs.unlink(filePath(notesdir, key)); //La fs.unlinkfunción borra nuestro archivo
}

export async function keylist() {  // leer cada archivo en ese directorio
    var notesdir = await notesDir();//verifica que el directorio exista
    var filez = await fs.readdir(notesdir); //lee el archivo del directorio seleccionado
    if (!filez || typeof filez === 'undefined') filez = []; // pre. si esta vacio o no existe genera una matriz
    var thenotes = filez.map(async fname => { //recorra la mariz y construye una nueva matriz de una matriz existente
        var key = path.basename(fname, '.json');
        var thenote = await readJSON(notesdir, key);
        return thenote.key; 
    }); 
    return Promise.all(thenotes); 
}

export async function count() { //Contar el número de objetos notas 
    var notesdir = await notesDir();
    var filez = await fs.readdir(notesdir); 
    return filez.length;
}

export async function close() { }