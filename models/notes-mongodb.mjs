import util from 'util';
import Note from './Note';
import mongodb from 'mongodb'; 
const MongoClient = mongodb.MongoClient;
import DBG from 'debug';
const debug = DBG('notes:notes-mongodb'); 
const error = DBG('notes:error-mongodb'); 

var client;

async function connectDB() { 
    if (!client) client = await MongoClient.connect(process.env.MONGO_URL, { useNewUrlParser: true }); //conecta a mongo mediante url
    return { 
        db: client.db(process.env.MONGO_DBNAME),  
        client: client
    };
}
/*
La MongoClientclase se utiliza para conectarse con una instancia de MongoDB. La URL requerida, que se especifica 
mediante una variable de entorno, utiliza un formato sencillo: mongodb://localhost/. El nombre de la base de 
datos se especifica a través de otra variable de entorno.

Esto crea el cliente de la base de datos y luego abre la conexión de la base de datos. Ambos objetos se 
devuelven desde connectDBun objeto anónimo  
*/
export async function create(key, title, body) { 
    const { db, client } = await connectDB();
    const note = new Note(key, title, body); 
    const collection = db.collection('notes'); 
    await collection.insertOne({ notekey: key, title, body });
    return note;
}
 
export async function update(key, title, body) { 
    const { db, client } = await connectDB();
    const note = new Note(key, title, body); 
    const collection = db.collection('notes'); 
    await collection.updateOne({ notekey: key }, { $set: { title, body } });
    return note;
}

export async function read(key) { 
    const { db, client } = await connectDB();
    const collection = db.collection('notes');
    const doc = await collection.findOne({ notekey: key });
    const note = new Note(doc.notekey, doc.title, doc.body);
    return note; 
}
/*
El mongodbcontrolador soporta varias variantes de findoperaciones. En este caso, la aplicación de Notas
 garantiza que exista exactamente un documento que coincida con una clave determinada. Por lo tanto, podemos
  utilizar el findOnemétodo. Como su nombre lo indica, findOnedevolverá el primer documento coincidente.

  El argumento a findOnees un descriptor de consulta. Esta simple consulta busca documentos cuyo notekeycampo 
  coincida con el solicitado key. Por supuesto, una consulta vacía coincidirá con todos los documentos de la colección. 
  Puede coincidir con otros campos de una manera similar, y el descriptor de la consulta puede hacer mucho más.
 * 
 */
export async function destroy(key) { 
    const { db, client } = await connectDB();
    const collection = db.collection('notes'); 
    await collection.findOneAndDelete({ notekey: key });
}

/*
 Una de las findvariantes es findOneAndDelete. Como su nombre lo indica, encuentra un documento que coincide con
  el descriptor de la consulta y luego lo elimina 
 */
export async function keylist() { 
    const { db, client } = await connectDB();
    const collection = db.collection('notes'); 
    const keyz = await new Promise((resolve, reject) => { 
        var keyz = []; 
        collection.find({}).forEach( 
            note => { keyz.push(note.notekey); }, 
            err => { 
                if (err) reject(err); 
                else resolve(keyz); 
            } 
        ); 
    }); 
    return keyz;
}
/*
Aquí, estamos usando la findoperación base y le damos una consulta vacía para que coincida con todos
los documentos. Lo que vamos a devolver es una matriz que contiene notekeypara cada documento.
  
El objeto Cursor es, como su nombre lo indica, un puntero a un conjunto de resultados de una consulta.
Tiene una serie de funciones útiles relacionadas con la operación en un conjunto de resultados. Por ejemplo,
puede omitir los primeros elementos de los resultados, o limitar el tamaño del conjunto de resultados, o 
realizar las operaciones filtery map.

El Cursor.forEachmétodo toma dos funciones de devolución de llamada. El primero se llama en cada elemento
 del conjunto de resultados. En este caso, podemos usar eso para grabar solo notekeyen una matriz. Se llama
  a la segunda devolución de llamada después de que se hayan procesado todos los elementos del conjunto de 
  resultados. Usamos esto para indicar el éxito o el fracaso, y para devolver la keyzmatriz.

Debido a que forEachutiliza este patrón, no tiene una opción para proporcionar una Promesa, y tenemos que
 crear la Promesa nosotros mismos, como se muestra
 */

export async function count() { 
    const { db, client } = await connectDB();
    const collection = db.collection('notes');
    const count = await collection.count({});
    return count;
}

export async function close() {
    if (client) client.close();
    client = undefined;
}