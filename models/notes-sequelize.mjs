import fs from 'fs-extra';
import util from 'util';
import jsyaml from 'js-yaml';
import Note from './Note';
import Sequelize from 'sequelize';
import DBG from 'debug';
const debug = DBG('notes:notes-sequelize'); 
const error = DBG('notes:error-sequelize'); 

var SQNote; //modelo de datos describe nuestra estructura de objetos a sequializar
var sequlz; // objeto se almacena La conexión de la base de datos 

async function connectDB() {
    if (typeof sequlz === 'undefined') {
        const YAML = await fs.readFile(process.env.SEQUELIZE_CONNECT, 'utf8'); // parame
        const params = jsyaml.safeLoad(YAML, 'utf8');
        console.log(params);
        sequlz = new Sequelize(params.dbname, params.username, params.password, params.params);
    }
    if (SQNote) return SQNote.sync(); // forzado sincrono sequelize
    SQNote = sequlz.define('Note', { //define el esquema de la BD,Sequelize mapea los atributos del objeto en columnas en tablas
        notekey: {
            type: Sequelize.STRING,
            primaryKey: true, unique: true
        },
        title: Sequelize.STRING,
        body: Sequelize.TEXT
    });
    return SQNote.sync();
}
/*
Los parámetros de conexión Sequelize se almacenan en un archivo YAML que especificamos en la SEQUELIZE_CONNECTvariable
de entorno. La línea new Sequelize(..)abre la conexión de la base de datos. Los parámetros obviamente contienen cualquier
nombre de base de datos, nombre de usuario, contraseña y otras opciones necesarias para conectarse con la base de datos.

La línea sequlz.definees donde definimos el esquema de la base de datos. En lugar de definir el esquema como el comando 
SQL para crear la tabla de la base de datos, ofrecemos una descripción de alto nivel de los campos y sus características.
 Sequelize mapea los atributos del objeto en columnas en tablas.  
 */

export async function create(key, title, body) { 
    const SQNote = await connectDB();
    const note = new Note(key, title, body); 
    await SQNote.create({ notekey: key, title: title, body: body });
    return note;
}
 
export async function update(key, title, body) { 
    const SQNote = await connectDB();
    const note = await SQNote.find({ where: { notekey: key } }) 
    if (!note) { throw new Error(`No note found for ${key}`); } else { 
        await note.updateAttributes({ title: title, body: body });
        return new Note(key, title, body);
    } 
}

/**
Hay varias formas de crear una nueva instancia de objeto en Sequelize. Lo más simple es llamar a la createfunción
de un objeto (en este caso, SQNote.create). Esa función colapsa entre sí otras dos funciones, build(para crear el objeto),
 y save(para escribirlo en la base de datos).

Actualizar una instancia de objeto es un poco diferente. Primero, debemos recuperar su entrada de la base de datos utilizando
la findoperación. A la findoperación se le da un objeto que especifica la consulta. Usando  find, recuperamos una instancia,
 mientras que la findAlloperación recupera todas las instancias coincidentes.

Para obtener documentación sobre las consultas de Sequelize, visite http://docs.sequelizejs.com/en/latest/docs/querying/ .

Como la mayoría o todas las demás funciones de Sequelize,  SQNote.finddevuelve una Promesa. Por lo tanto, dentro de una
 asyncfunción, somos awaitel resultado de la operación. 

La operación de actualización requiere dos pasos, el primero es findel objeto correspondiente para leerlo desde la base 
de datos. Una vez que se encuentra la instancia, podemos actualizar sus valores simplemente con la
updateAttributesfunción
 */

export async function read(key) { 
    const SQNote = await connectDB(); //CONECT DB
    const note = await SQNote.find({ where: { notekey: key } }) // FIND IN THE DB 
    if (!note) { throw new Error(`No note found for ${key}`); } else { 
        return new Note(note.notekey, note.title, note.body); 
    } 
}

/**
Para leer una nota, usamos la findoperación nuevamente. Existe la posibilidad de un resultado vacío, y tenemos que
 lanzar un error para que coincida.

El contrato para esta función es devolver un objeto tipo Note. Eso significa tomar los campos recuperados usando Sequelize
 y usar eso para crear un Noteobjeto
 */

export async function destroy(key) { 
    const SQNote = await connectDB();
    const note = await SQNote.find({ where: { notekey: key } }) 
    return note.destroy(); 
}
/**
 Para destruir una nota, usamos la operación find para recuperar su instancia, y luego llamamos a su destroy()método
 */

export async function keylist() { 
    const SQNote = await connectDB();
    const notes = await SQNote.findAll({ attributes: [ 'notekey' ] });
    return notes.map(note => note.notekey); 
}
/**
Debido a que la keylistfunción actúa sobre todos los Noteobjetos, usamos la findAlloperación. Buscamos el
notekeyatributo en todas las notas. Se nos da una matriz de objetos con un campo llamado notekey, y 
usamos la .mapfunción para convertir esto en una matriz de las claves de nota:
 */

export async function count() { 
    const SQNote = await connectDB();
    const count = await SQNote.count(); //el count()método para calcular el resultado necesario
    return count; 
}

export async function close() {
    if (sequlz) sequlz.close();
    sequlz = undefined;
    SQNote = undefined;
}
