import util from 'util';
import Note from './Note';
import sqlite3 from 'sqlite3';
import DBG from 'debug';
const debug = DBG('notes:notes-sqlite3'); 
const error = DBG('notes:error-sqlite3'); 

var db; // store the database connection here 
 
async function connectDB() { 
    if (db) return db; 
    var dbfile = process.env.SQLITE_FILE || "notes.sqlite3"; 
    await new Promise((resolve, reject) => {
        db = new sqlite3.Database(dbfile, 
            sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, 
            err => { 
                if (err) return reject(err); 
                resolve(db);
        });
    });
    return db;
}
// administrar la conexión de la base de datos. Si la base de datos no está abierta, seguirá adelante e incluso se
//asegurará de que se cree el archivo de la base de datos (si no existe). Pero si ya está abierto, se devuelve inmediatamente



export async function create(key, title, body) { 
    var db = await connectDB();
    var note = new Note(key, title, body); 
    await new Promise((resolve, reject) => { 
        db.run("INSERT INTO notes ( notekey, title, body) "+ 
            "VALUES ( ?, ? , ? );", [ key, title, body ], err => { 
                if (err) return reject(err); 
                resolve(note); 
        }); 
    });
    return note;
}
 
export async function update(key, title, body) { 
    var db = await connectDB();
    var note = new Note(key, title, body); 
    await new Promise((resolve, reject) => { 
        db.run("UPDATE notes "+ 
            "SET title = ?, body = ? WHERE notekey = ?", 
            [ title, body, key ], err => { 
                if (err) return reject(err); 
                resolve(note); 
        }); 
    });
    return note;
}
//La db.run función simplemente ejecuta la consulta SQL que se le da, y no recupera ningún dato. Debido a que 
//el sqlite3módulo no produce ningún tipo de Promesa, tenemos que ajustar las llamadas de función en un Promiseobjeto


export async function read(key) {
    var db = await connectDB();
    var note = await new Promise((resolve, reject) => {
      db.get("SELECT * FROM notes WHERE notekey = ?", [key], (err,row) => {
          if (err) return reject(err);
          const note = new Note(row.notekey, row.title, row.body);
          resolve(note);
       });
    });
    return note;
  }
  //usar db.getpara recuperar una nota es suficiente porque solo hay una nota por notekey. Por lo tanto,
  //nuestra SELECTconsulta volverá a lo más una fila de todos modos


  export async function destroy(key) {
    var db = await connectDB();
    return await new Promise((resolve, reject) => {
      db.run("DELETE FROM notes WHERE notekey = ?;", [key], err => {
          if (err) return reject(err);
          resolve();
      });
    });
  }//Para destruir una nota, simplemente ejecutamos la DELETE FROM declaración

  export async function keylist() {
    var db = await connectDB();
    var keyz = await new Promise((resolve, reject) => {
        var keyz = [];
        db.all("SELECT notekey FROM notes", (err, rows) => {
                if (err) return reject(err);
                resolve(rows.map(row => row.notekey ));
            });
    });
    return keyz;
}
/*
Esta funcion se encarga devolver una matriz de teclas de nota. El objeto rows es una matriz de resultados 
de la base de datos que contiene los datos que debemos devolver, pero en un formato diferente. Por lo tanto,
 usamos la map función para convertir la matriz al formato requerido para cumplir el contrato:
 */

export async function count() {
    var db = await connectDB();
    var count = await new Promise((resolve, reject) => {
        db.get("select count(notekey) as count from notes",(err, row) => {
                if (err) return reject(err);
                resolve(row.count);
            });
    });
    return count;
}

/*
Simplemente podemos usar SQL para contar el número de notas para nosotros. En este caso, 
db.getdevuelve una fila con una sola columna count, que es el valor que queremos devolver.
 */
export async function close() {
    var _db = db;
    db = undefined;
    return _db ? new Promise((resolve, reject) => {
            _db.close(err => {
                if (err) reject(err);
                else resolve();
            });
        }) : undefined;
}

