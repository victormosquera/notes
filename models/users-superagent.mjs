
// ---------------MODELO DE DATOS DE USUARIO  -------------------------------------


import request from 'superagent';
import util from 'util';
import url from 'url';
const URL = url.URL;
import DBG from 'debug';
const debug = DBG('notes:users-superagent');
const error = DBG('notes:error-superagent');

function reqURL(path) {
    const requrl = new URL(process.env.USER_SERVICE_URL); //
    requrl.pathname = path;//añade el pathname a al url del usuario
    return requrl.toString(); // retorna la url stringsificada con el path (http://localhost:3333/path)
}
/*
 Con superagent, no dejamos una conexión abierta al servicio, sino que abrimos una nueva conexión en 
 cada solicitud. Lo más común es formular la URL de solicitud. Se espera que el usuario proporcione 
 una URL base, como por ejemplo  http://localhost:3333/, en la USER_SERVICE_URLvariable de entorno. 
 
 Esta función modifica esa URL, utilizando el nuevo soporte de URL WHATWG en Node.js, para usar una ruta
  de URL determinada:
 */


export async function create(username, password, provider, familyName, givenName, middleName, emails, photos) {
    var res = await request //abrimos una solicitud superagent 
        .post(reqURL('/create-user')) 
        .send({username, password, provider, familyName, givenName, middleName, emails, photos })
        .set('Content-Type', 'application/json')
        .set('Acccept', 'application/json')
        .auth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');
    return res.body;
}

export async function update(username, password, provider, familyName, givenName, middleName, emails, photos) {
    var res = await request
        .post(reqURL(`/update-user/${username}`))
        .send({ username, password, provider, familyName, givenName, middleName, emails, photos})
        .set('Content-Type', 'application/json')
        .set('Acccept', 'application/json')
        .auth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');
    return res.body;
}

/**
En este caso, hemos elegido deliberadamente nombres de variables para que los parámetros coincidan con los nombres
de campo del objeto con los nombres de parámetros utilizados por el servidor. Al hacerlo, podemos usar esta notación
abreviada para objetos anónimos, y nuestro código es un poco más limpio al usar nombres de variables consistentes
de principio a fin
 */
export async function find(username) { //busca info del usuario deseado
    var res = await request
        .get(reqURL(`/find/${username}`))
        .set('Content-Type', 'application/json')
        .set('Acccept', 'application/json')
        .auth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');
    return res.body;
}

export async function userPasswordCheck(username, password) { 
    var res = await request
        .post(reqURL(`/passwordCheck`))
        .send({ username, password })
        .set('Content-Type', 'application/json')
        .set('Acccept', 'application/json')
        .auth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');
    return res.body;
} 
/*
Estamos enviando la solicitud para comprobar las contraseñas al servidor. 

Un punto sobre este método es útil señalar. Podría haber tomado los parámetros en la URL, en lugar del
cuerpo de la solicitud como se hace aquí. Pero como la URL de solicitud se registra de forma rutinaria
en los archivos, colocar los parámetros de nombre de usuario y contraseña en la URL significa que la
información de identidad del usuario se registrará en los archivos y parte de los informes de actividad.
Eso obviamente sería una muy mala elección. Poner esos parámetros en el cuerpo de la solicitud no
solo evita ese mal resultado, sino que si se utilizara una conexión HTTPS con el servicio, la 
transacción se cifraría:
 */

export async function findOrCreate(profile) {  
    var res = await request
        .post(reqURL('/find-or-create'))
        .send({ 
            username: profile.id,
            password: profile.password, 
            provider: profile.provider, 
            familyName: profile.familyName, 
            givenName: profile.givenName, 
            middleName: profile.middleName, 
            emails: profile.emails,
            photos: profile.photos 
        })
        .set('Content-Type', 'application/json')
        .set('Acccept', 'application/json')
        .auth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');
    return res.body;
}

export async function listUsers() { 
    var res = await request
        .get(reqURL('/list'))
        .set('Content-Type', 'application/json')
        .set('Acccept', 'application/json')
        .auth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF'); 
    return res.body;
}