import util from 'util';
import express from 'express';
import * as notes from '../models/notes';

export const router = express.Router(); 
 
/* GET home page. */
router.get('/', async (req, res, next) => {
  try {
    let keylist = await notes.keylist();
    let keyPromises = keylist.map(key => { return notes.read(key) });
    let notelist = await Promise.all(keyPromises); // si una de las promesas falla, promise all falla
    res.render('index', { 
      title: 'Notes',
      notelist: notelist,
      user: req.user ? req.user : undefined
    });
  } catch (e) { next(e); }
});