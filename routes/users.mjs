
// ---------------AUTENTICACIÓN DEL USUARIO  -------------------------------------

import path from 'path';
import util from 'util';
import express from 'express'; 
import passport from 'passport'; 
import passportLocal from 'passport-local';
const LocalStrategy = passportLocal.Strategy;  
import * as usersModel from '../models/users-superagent';//modelo de autenticacion basada en REST
import { sessionCookieName } from '../app';

export const router = express.Router();

import DBG from 'debug';
const debug = DBG('notes:router-users'); 
const error = DBG('notes:error-users'); 

export function initPassport(app) { 
    app.use(passport.initialize()); 
    app.use(passport.session()); 
}
   
export function ensureAuthenticated(req, res, next) { 
    try {
        // req.user está configurado por Passport en la función de deserialización 
        if (req.user) next(); 
        else res.redirect('/users/login'); 
    } catch (e) { next(e); }
}
/**
usaremos sesiones Passport para detectar si esta solicitud HTTP está autenticada o no. Examina todas
las solicitudes que llegan a la aplicación, busca pistas sobre si este navegador está conectado o no,
y adjunta datos al objeto de solicitud como req.user.

La ensureAuthenticatedfunción será utilizada por otros módulos de enrutamiento y se insertará en 
cualquier definición de ruta que requiera un usuario registrado registrado. Por ejemplo, editar o 
eliminar una nota requiere que el usuario esté conectado y, por lo tanto, las rutas correspondientes
routes/notes.mjsdeben usarse ensureAuthenticated. Si el usuario no ha iniciado sesión, esta función
los redirecciona para /users/loginque puedan hacerlo
 */

router.get('/login', function(req, res, next) { 
    try {
      res.render('login', { title: "Login to Notes", user: req.user, }); 
    } catch (e) { next(e); }
  }); 
   
router.post('/login', 
  passport.authenticate('local', { 
      successRedirect: '/', // SUCCESS: Go to home page 
      failureRedirect: 'login', // FAIL: Go to /user/login 
      failureFlash: 'Invalid username or password.',
      successFlash: 'Welcome !'
    }) 
  )
/**
Debido a que este enrutador está montado /users, todas estas rutas se habrán /userprefijado. 
La /users/loginruta simplemente muestra un formulario solicitando un nombre de usuario y contraseña.
Cuando se envía este formulario, aterrizamos en la segunda declaración de ruta, con un POSTon  /users/login.
Si passportconsidera que este es un intento de inicio de sesión exitoso LocalStrategy, entonces el navegador 
se redirige a la página de inicio. De lo contrario, se redirige a la  /users/loginpágina:
 */

router.get('/logout', function(req, res, next) { 
    try {
        req.session.destroy();//borra las credenciales de inicio de sesion
        req.logout(); 
        res.clearCookie(sessionCookieName);//borra datos de la cookie
        res.redirect('/'); 
   } catch (e) { next(e); }
  });

  /**
La req.logoutfunción le indica a Passport que borre sus credenciales de inicio de sesión y luego se redirigen
 a la página de inicio.

Esta función se desvía de lo que está en la documentación de Passport. Allí, se nos dice que simplemente llamemos 
req.logout. Pero llamar solo a esa función a veces hace que el usuario no cierre la sesión. Es necesario destruir 
el objeto de la sesión y borrar la cookie para garantizar que el usuario se desconecta. El nombre de la cookie 
se define en app.mjs, e importamos  sessionCookieNamepara esta función
   */

passport.use( new LocalStrategy( async (username, password, done) => { 
        try {
        var check = await usersModel.userPasswordCheck(username, password);
        if (check.check) { 
            done(null, { id: check.username, username: check.username }); 
        } else { 
            done(null, false, check.message); 
        } 
        } catch (e) { done(e); }
    } 
)); 
/**
 Aquí es donde definimos nuestra implementación de LocalStrategy. En la función de devolución de
  llamada usersModel.userPasswordCheck, llamamos , lo que hace una llamada REST al servicio de 
  autenticación de usuario. Recuerde que esto realiza la verificación de la contraseña y luego
   devuelve un objeto que indica si han iniciado sesión o no.

Un inicio de sesión exitoso se indica cuando check.checkes true. Para este caso, le pedimos a Passport
 que use un objeto que contenga el objeto usernamede la sesión. De lo contrario, tenemos dos formas de 
 decirle a Passport que el intento de inicio de sesión no tuvo éxito. En un caso, usamos done(null, false)
 para indicar un inicio de sesión de error y para transmitir el mensaje de error que recibimos. 
 En el otro caso, habremos capturado una excepción y pasaremos esa excepción.

Notarás que Passport usa una API de estilo de devolución de llamada. Passport proporciona una donefunción,
 y debemos llamarla cuando sepamos qué es qué. Si bien usamos una asyncfunción para realizar una llamada 
 asíncrona limpia al servicio backend, Passport no sabe cómo asimilar la Promesa que se devolvería. Por 
 lo tanto, tenemos que lanzar un círculo try/catchalrededor del cuerpo de la función para detectar cualquier 
 excepción lanzada
 */

passport.serializeUser(function(user, done) { 
try {
    done(null, user.username); 
} catch (e) { done(e); }
}); 

passport.deserializeUser(async (username, done) => { 
try {
    var user = await usersModel.find(username);
    done(null, user);
} catch(e) { done(e); }
}); 

/**
Se llama al objetoobjeto mientras se procesa una solicitud HTTP entrante y es donde buscamos
los datos del perfil de usuario. Pasaporte adjuntará esto al objeto de solicitud
 */